﻿using System;
/*
* AUTHOR: Didac Sanchez Cullell
* DATE: 2022/11/24 - 2022/11/30
* DESCRIPTION: Exercicis Matrius
*/

namespace M03Uf1VectorsSanchezCullell
{
    class Matrius
    {
        //Enfonsar la flota, indica si a la posició (x, y) hi ha aigua o un vaixell (tocat)
        public void SimpleBattleshipResult()
        {
            Console.Clear();
            int tocat = 0;
            int cordY;
            int cordX;
            char[,] mar = { { 'x', 'x', '0', '0', '0', '0', 'x', '0' }, { '0', '0', 'x', '0', '0', '0', 'x', '0' }, { '0', '0', '0', '0', '0', '0', 'x', '0' }, { '0', 'x', 'x', 'x', '0', '0', 'x', '0' }, { '0', '0', '0', '0', 'x', '0', '0', '0' }, { '0', '0', '0', '0', 'x', '0', '0', '0' }, { 'x', '0', '0', '0', '0', '0', '0', '0' } };
            for (int i = 0; i < mar.GetLength(0); i++)
            {
                for (int j = 0; j < mar.GetLength(1); j++)
                {
                    Console.Write(mar[i, j]);
                    if (j == mar.GetLength(1) - 1) Console.WriteLine("");
                }
            }
            do
            {
                do
                {
                    Console.WriteLine("La cordenada Y");
                    cordY = Convert.ToInt32(Console.ReadLine());
                } while (cordY < 0 || cordY > mar.GetLength(0));
                do
                {
                    Console.WriteLine("La cordenada X");
                    cordX = Convert.ToInt32(Console.ReadLine());
                } while (cordX < 0 || cordX > mar.GetLength(1));
                if (mar[cordY, cordX] == 'x')
                {
                    Console.WriteLine("Tocat");
                    mar[cordY, cordX] = '*';
                    tocat++;
                }
                else
                {
                    Console.WriteLine("Aigua");
                }
            } while (tocat < 13);
            Console.WriteLine("Has guanyat");
            Console.ReadLine();
            Console.Clear();

        }
        //Imprimeix la suma de tots els valors de la matriu {{2,5,1,6},{23,52,14,36},{23,75,81,64}}.
        public void MatrixElementSum()
        {
            Console.Clear();
            int resultat = 0;
            int[,] matriu = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(1); j++)
                {
                    resultat += matriu[i, j];
                }
            }
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(1); j++)
                {
                    Console.Write($"{ matriu[i, j]}\t");
                    if (j == matriu.GetLength(1) - 1 && i != matriu.GetLength(0) / 2) Console.WriteLine("");
                }
                if (i == matriu.GetLength(0) / 2) Console.WriteLine($"\t=\t{resultat}");
            }
            Console.ReadLine();
            Console.Clear();
        }
        //Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte. L'usuari introduirà parells d'enters del 0 al 3 quan s'obri la caixa indicada. Quan introdueixi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        public void MatrixBoxesOpenedCounter()
        {
            Console.Clear();
            int[,] cajas = new int[4, 4];
            int y;
            int x;
            for (int i = 0; i < cajas.GetLength(0); i++) for (int j = 0; j < cajas.GetLength(1); j++) cajas[i, j] = 0;
            do
            {
                Console.WriteLine("Numero de caixa oberta");
                y = Convert.ToInt32(Console.ReadLine());
                x = Convert.ToInt32(Console.ReadLine());
                if ((y > 0 && y < cajas.GetLength(0) - 1) && (x > 0 && x < cajas.GetLength(1) - 1)) cajas[y, x]++;
            } while (y != -1 || x != -1);
            for (int i = 0; i < cajas.GetLength(0); i++)
            {
                for (int j = 0; j < cajas.GetLength(1); j++)
                {
                    Console.Write(cajas[i, j]);
                    if (j == cajas.GetLength(1) - 1) Console.WriteLine("");
                }
            }
            Console.ReadLine();
            Console.Clear();
        }
        // matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,62}} Imprimeix true si algún dels números és divisible entre 13, false altrement.
        public void MatrixThereADiv13()
        {
            Console.Clear();
            int[,] matrix = { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 62 } };
            bool flag = false;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] % 13 == 0)
                    {
                        flag = true;
                        break;
                    }

                }
            }
            Console.WriteLine($"Es {flag} que hi hagi un numero multiple de 13");
            Console.ReadLine();
            Console.Clear();
        }
        // Digues en quin punt(x,y) es troba el cim més alt i la seva alçada map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}}
        public void HighestMountainOnMap()
        {
            Console.Clear();
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            int maxY = 0, maxX = 0;
            double maxPic = map[0, 0];
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 1; j < map.GetLength(1); j++)
                {
                    if (map[i, j] > maxPic)
                    {
                        (maxY, maxX) = (i, j);
                        maxPic = map[i, j];
                    }
                }
            }
            Console.WriteLine($"El punt mes alt es {map[maxY, maxX]} en las cordenades {maxY},{maxX}");
            Console.ReadLine();
            Console.Clear();
        }
        // Camvia les dades de l'exercici anterior de m a peus
        public void HighestMountainScaleChange()
        {
            Console.Clear();
            double[,] map = { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    map[i, j] *= 3.2808;
                }
            }
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    Console.Write($"{Math.Round(map[i, j])} \t");
                    if (j == map.GetLength(1) - 1) Console.WriteLine("");
                }
            }
        }
        //Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues matrius.
        public void MatrixSum()
        {
            Console.Clear();
            Console.WriteLine("Nº de columnes");
            int numCulomnesPrimera = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nº de files");
            int numFilesPrimera = Convert.ToInt32(Console.ReadLine());
            int[,] matriuPrimera = new int[numCulomnesPrimera, numFilesPrimera];
            for (int i = 0; i < matriuPrimera.GetLength(0); i++)
            {
                for (int j = 0; j < matriuPrimera.GetLength(1); j++)
                {
                    Console.WriteLine($"un valor per la posicio {i},{j}");
                    matriuPrimera[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            Console.WriteLine("Nº de columnes");
            int numCulomnesSegona = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nº de files");
            int numFilesSegona = Convert.ToInt32(Console.ReadLine());
            int[,] matriuSegona = new int[numCulomnesSegona, numFilesSegona];
            for (int i = 0; i < matriuSegona.GetLength(0); i++)
            {
                for (int j = 0; j < matriuSegona.GetLength(1); j++)
                {
                    Console.WriteLine($"un valor per la posicio {i},{j}");
                    matriuSegona[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            if (numCulomnesPrimera == numCulomnesSegona && numFilesPrimera == numFilesSegona)
            {
                int[,] matriuResultat = new int[numCulomnesSegona, numFilesSegona];
                for (int i = 0; i < matriuSegona.GetLength(0); i++)
                {
                    for (int j = 0; j < matriuSegona.GetLength(1); j++)
                    {
                        matriuResultat[i, j] = matriuPrimera[i, j] + matriuSegona[i, j];
                        Console.Write($"{matriuResultat[i, j]}\t");
                        if (j == matriuSegona.GetLength(1) - 1) Console.WriteLine("");
                    }
                }
            }
            else
            {
                Console.WriteLine("No espoden sumar");
            }
            Console.ReadLine();
            Console.Clear();
        }
        // Programa una funció que donat un tauler d'escacs, i una posició, ens mostri per pantalla quines són les possibles posicions a les que es pot moure una torre.
        public void RookMoves()
        {
            Console.Clear();
            string lletra;
            int lletraNum = 0, cordenada;
            string[,] tauler = new string[8, 8];
            for (int i = 0; i < tauler.GetLength(0); i++)
            {
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    tauler[i, j] = "x";
                }
            }
            Console.WriteLine("En quina cordenada esta la torre");
            do
            {
                lletra = Console.ReadLine();
            } while (lletra != "a" && lletra != "b" && lletra != "c" && lletra != "d" && lletra != "e" && lletra != "f" && lletra != "g" && lletra != "h");
            switch (lletra)
            {
                case "a":
                    lletraNum = 0;
                    break;
                case "b":
                    lletraNum = 1;
                    break;
                case "c":
                    lletraNum = 2;
                    break;
                case "d":
                    lletraNum = 3;
                    break;
                case "e":
                    lletraNum = 4;
                    break;
                case "f":
                    lletraNum = 5;
                    break;
                case "g":
                    lletraNum = 6;
                    break;
                case "h":
                    lletraNum = 7;
                    break;
            }
            do
            {
                cordenada = Convert.ToInt32(Console.ReadLine());
            } while (cordenada < 1 && cordenada > 8);
            for (int i = 0; i < tauler.GetLength(0); i++)
            {
                tauler[cordenada - 1,i ] = "♖";
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    tauler[j, lletraNum] = "♖";
                }
            }
            tauler[cordenada - 1, lletraNum] = "♜";
            for (int i = 0; i < tauler.GetLength(0); i++)
            {
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    Console.Write($"{tauler[i, j]}\t");
                    if (j == tauler.GetLength(1) - 1) Console.WriteLine("");
                }

            }
            Console.ReadLine();
            Console.Clear();
        }
        // Donada una matriu quadrada, el programa imprimeix true si la matriu és simètrica, false en cas contrari
        public void MatrixSimetric()
        {
            Console.Clear();
            bool flag = true;
            Console.WriteLine("Dimensio de la matriu");
            int dimensions = Convert.ToInt32(Console.ReadLine());
            int[,] matriu = new int[dimensions, dimensions];
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(1); j++)
                {
                    Console.WriteLine($"un valor per la posicio {i},{j}");
                    matriu[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j <matriu.GetLength(1); j++)
                {
                    if (matriu[i,j] != matriu[j, i])
                    {
                        flag = false;
                        break;
                    }
                }
            }
            Console.WriteLine($"Es {flag} que la matriu es simetrica");
            Console.ReadLine();
            Console.Clear();
        }
        public void MultiplicacióDeMatrius()
        {
            // Les matrius es poden multiplicar entre elles mentre siguin de quadrades de igual dimensió o interc
            Console.Clear();
        }
        public void QueenGame()
        {
            // En una matriu de 64 caselles (8x8) que representa un taulell d’escacs s’ha de posicionar 8 reines que no es matin entre elles.
            Console.Clear();
        }
        public void CrearUnQuadreMagic()
        {
            /*
             *   Donada una matriu quadrada la ompliu de valors enters on:
             *   La suma dels valors de qualsevol fila donen el mateix valor S
             *   La suma dels valors de qualsevol columna dona el matiex valor S
             *   La suma dels valors de qualsevol diagonal dona el mateix valor S
             */
            Console.Clear();

        }
        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.SimpleBattleshipResult");
                Console.WriteLine("2.MatrixElementSum");
                Console.WriteLine("3.MatrixBoxesOpenedCounter");
                Console.WriteLine("4.MatrixThereADiv13");
                Console.WriteLine("5.HighestMountainOnMap");
                Console.WriteLine("6.HighestMountainScaleChange");
                Console.WriteLine("7.MatrixSum");
                Console.WriteLine("8.RookMoves");
                Console.WriteLine("9.MatrixSimetric");
                Console.WriteLine("10.MultiplicacióDeMatrius");
                Console.WriteLine("11.QueenGame");
                Console.WriteLine("12.CrearUnQuadreMàgic");
                opcio = Console.ReadLine();
                switch (opcio)
                {
                    case "0":
                        break;
                    case "1":
                        SimpleBattleshipResult();
                        break;
                    case "2":
                        MatrixElementSum();
                        break;
                    case "3":
                        MatrixBoxesOpenedCounter();
                        break;
                    case "4":
                        MatrixThereADiv13();
                        break;
                    case "5":
                        HighestMountainOnMap();
                        break;
                    case "6":
                        HighestMountainScaleChange();
                        break;
                    case "7":
                        MatrixSum();
                        break;
                    case "8":
                        RookMoves();
                        break;
                    case "9":
                        MatrixSimetric();
                        break;
                    case "10":
                        MultiplicacióDeMatrius();
                        break;
                    case "11":
                        QueenGame();
                        break;
                    case "12":
                        CrearUnQuadreMagic();
                        break;

                    default:
                        Console.WriteLine("Opcio incorecte");
                        Console.Clear();
                        break;

                }
            } while (opcio != "0");
        }
        static void Main()
        {
            var menu = new Matrius();
            menu.Menu();
        }
    }
}
